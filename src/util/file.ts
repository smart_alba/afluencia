import *  as fs from "fs";
import { Log } from "./log";

class Service {
    static class = "FileUtil";

    constructor() {
    }

    public static async saveFile(path: string, data: any) {
        return new Promise(async (resolve, reject) => {
            const writeData: string = Service.convertToCSV(data);
            if (!writeData) {
                return resolve();
            }
            fs.writeFile(process.env.OUT_PATH + path, writeData, 'utf8', function (err) {
                if (err) {
                    Log.error(err.stack, Service.class, 'saveFile');
                    return reject();
                }
                return resolve();
            });
        });
    };

    static convertToCSV(objArray: any) {
        const array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        if (!array.length) {
            return "";
        }
        const cabeceras = Object.keys(array[0]).sort();
        let str = '\"' + cabeceras.join('"' + process.env.OUT_SEPARATOR + '"') + '\"\r\n';

        for (let i = 0; i < array.length; i++) {
            let line = '';
            for (let key of cabeceras) {
                if (line != '') line += process.env.OUT_SEPARATOR;
                line += '"' + array[i][key] + '"';
            }
            str += line + '\r\n';
        }
        return str;
    }
}


export let FileUtil = Service;