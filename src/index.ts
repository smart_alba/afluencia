import * as dotenv from "dotenv";

import { Log } from "./util/log";
import { AuthClass } from "./api/auth";
import { ReceptorClass } from "./api/receptor";
import { FileUtil } from "./util/file";


dotenv.config({path: ".env"});

const config = {
    domain: process.env.DOMAIN,
    username: process.env.USER,
    password: process.env.PASSWORD
};

function getReceptors() {
    return new Promise(async (resolve, reject) => {
        const token: any = await AuthClass.login(config);
        const receptores: any = await ReceptorClass.getReceptores(config, token);
        const status: any = await ReceptorClass.getStatus(config, token);
        const detections: any = await ReceptorClass.getDetections(config, token);
        const data: any = ReceptorClass.mergeData(receptores, status, detections);
        try {
            await FileUtil.saveFile('receptores.csv', data);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Receptores');
        }
        resolve(data);
    });
}

function getPermanencias() {
    return new Promise(async (resolve, reject) => {
        const token: any = await AuthClass.login(config);
        const data: any = await ReceptorClass.getPermanencias(config, token);
        try {
            await FileUtil.saveFile('permanencias.csv', data);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Permanencias');
        }
        resolve(data);
    });
}

function getRutas() {
    return new Promise(async (resolve, reject) => {
        const token: any = await AuthClass.login(config);
        const data: any = await ReceptorClass.getRoutes(config, token);
        try {
            await FileUtil.saveFile('routes.csv', data);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Routes');
        }
        resolve(data);
    });
}

function tasks() {
    getReceptors().then((data: any) => {
        console.log("Receptores: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });

    getPermanencias().then((data: any) => {
        console.log("Permanencias: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });
    getRutas().then((data: any) => {
        console.log("Rutas: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });
}

tasks();