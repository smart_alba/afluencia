import { Log } from "../util/log";
import { RequestService } from "../util/request";

class Auth {
    class = 'AuthClass';

    constructor() {
    }

    public async login(p: any) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/v2/auth/login';
            const body = {username: p.username, password: p.password};
            try {
                const data: any = await RequestService.POST(url, body);
                resolve(data.token);
            } catch (err) {
                Log.error(err, this.class, 'login');
                reject([]);
            }
        });
    }
}

export let AuthClass = new Auth();