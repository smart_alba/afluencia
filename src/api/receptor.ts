import { Log } from "../util/log";
import { RequestService } from "../util/request";
import * as moment from 'moment';

class Receptor {
    class = 'ReceptorClass';

    constructor() {
    }

    public async getReceptores(p: any, token: string) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/v2/secure/clients/' + p.username + '/locations/getlocations';
            try {
                const receptores: any = [];
                const data: any = await RequestService.GET(url, token);
                const routes: any = await this.getRoutes(p, token);
                const routesByReceptor = this.transposeRoutes(routes);
                data.forEach((receptor: any) => {
                    if (routesByReceptor[receptor.id]) {
                        routesByReceptor[receptor.id].forEach((route: string) => {
                            receptores.push({
                                ...receptor,
                                route: route
                            });
                        });
                    } else {
                        receptores.push(receptores.push({
                            ...receptor,
                            route: ''
                        }));
                    }
                });
                resolve(receptores);
            } catch (err) {
                Log.error(err, this.class, 'getReceptores');
                reject([]);
            }
        });
    }

    public async getStatus(p: any, token: string) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/v2/secure/clients/' + p.username + '/locations/status';
            try {
                const data: any = await RequestService.GET(url, token);
                if (data.status) {
                    return resolve([]);
                }
                resolve(data);
            } catch (err) {
                Log.error(err, this.class, 'getStatus');
                reject([]);
            }
        });
    }

    public async getDetections(p: any, token: string) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/v2/secure/clients/' + p.username + '/locations/detections';
            try {
                const data: any = await RequestService.GET(url, token);
                if (data.status) {
                    return resolve([]);
                }
                resolve(data);
            } catch (err) {
                Log.error(err, this.class, 'getDetections');
                reject([]);
            }
        });
    }

    public mergeData(r: any[], s: any[], d: any[]) {
        return r.map((receptor: any) => {
            const status = s.filter((status: any) => status.location_id === receptor.id)[0] || {};
            const detections = d.filter((dtection: any) => dtection.location_id === receptor.id)[0] || {};
            return {
                ...receptor,
                ...status,
                ...detections
            };
        }).map((receptor: any) => {
            const _r = receptor;
            delete _r.location_id;
            return _r;
        });
    }

    // Divide cada dato de permanencia en franjas horarias
    private splitPermanence(permanences: Array<any>, permanence: any): void {
        // Redondeo a la hora más cercana, guardo el offset en segundos
        let timestamp = permanence.input_timestamp;
        let ts = moment(timestamp);
        let offs = ts.minutes() * 60 + ts.seconds();
        // Si la permanencia desborda de una hora a la siguiente, vamos a crear
        // varias entradas, una por hora.
        let duration = parseInt(permanence.permanence_time);
        while (duration > 0) {
            permanence.input_timestamp = timestamp;
            permanence.hour = ts.hours();
            permanence.offs = offs;
            // Calculo si la sesión desborda la hora. Si es así, actualizo
            // valores para la proxima ejecucion del bucle.
            let psecs = duration;
            if (offs + psecs > 3600) {
                psecs = 3600 - offs;
                offs = 0;
                ts.add(psecs, 'seconds');
                timestamp = ts.format("YYYY-MM-DD hh:mm:ss")
            }
            duration -= psecs;
            permanence.permanence_time = psecs;
            permanences.push({...permanence});
        }
    }

    public async getPermanencias(p: any, token: string) {
        return new Promise(async (resolve, reject) => {
            let fecha = new Date()
            fecha.setDate(fecha.getDate() - 1); // estadisticas de ayer
            const url = p.domain + '/v2/secure/clients/' + p.username + '/locations/historic_permanence?date=' + fecha.toISOString().substring(0, 10);
            try {
                const receptores: any = await this.getReceptores(p, token);
                let m: any = new Map();
                for (const r of receptores) {
                    m[r.id] = r.description;
                }
                const permanences: any = [];
                const data: any = await RequestService.GET(url, token);
                if (data.status) {
                    return resolve([]);
                }
                data.forEach((location: any) => {
                    location.pemanence.forEach((current: any) => {
                        const lid = location.location_id;
                        current.location_id = lid;
                        current.location_name = m[lid];
                        this.splitPermanence(permanences, current);
                    });
                });
                resolve(permanences);
            } catch (err) {
                Log.error(err, this.class, 'getDetections');
                reject([]);
            }
        });
    }

    public async getRoutes(p: any, token: string) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/v2/secure/clients/' + p.username + '/routes/getroutes';
            try {
                const routes: any = await RequestService.GET(url, token);
                if (routes.status) {
                    return resolve([]);
                }
                return resolve(routes);
            } catch (err) {
                Log.error(err, this.class, 'getRoutes');
                reject([]);
            }
        });
    }

    public transposeRoutes(routes: any) {
        let locations: any = {};
        routes.forEach((route: any) => {
            route.locations.forEach((location: any) => {
                if (!locations[location]) {
                    locations[location] = [];
                }
                locations[location].push(route.id);
            });
        });
        return locations;
    }
}

export let ReceptorClass = new Receptor();
