# Afluencia
----------
## Requisitos
`npm install -g typescrip`

## Comandos

- `npm install`  to install dependencies
- `npm run tsc` to compile only
- `npm run serve` to test while develop
- `npm start` to run in prod
